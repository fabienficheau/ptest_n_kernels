#include "CL/cl.h"
#include "oclHelper.h"
#include "xcl.h"

#include <assert.h>
#include <cstring>

#include <algorithm>
#include <cstdio>
#include <string>
#include <vector>

using std::vector;
using std::find;

static const int DATA_SIZE = 256;

size_t offset[1] = {0};
size_t global[1] = {1};
size_t local[1] = {1};

// Wrap any OpenCL API calls that return error code(cl_int) with the below macro
// to quickly check for an error
#define OCL_CHECK(call)                                                        \
  do {                                                                         \
    cl_int err = call;                                                         \
    if (err != CL_SUCCESS) {                                                   \
      printf(__FILE__ ":%d: [ERROR] " #call " returned %s\n", __LINE__,        \
             oclErrorCode(err));                                               \
      exit(EXIT_FAILURE);                                                      \
    }                                                                          \
  } while (0);

// Checks OpenCL error codes
void check(cl_int err_code) {
  if (err_code != CL_SUCCESS) {
    printf("ERROR: %d\n", err_code);
    exit(EXIT_FAILURE);
  }
}

// An event callback function that prints the operations performed by the OpenCL
// runtime.
void event_cb(cl_event event, cl_int cmd_status, void *data) {
  cl_command_type command;
  clGetEventInfo(event, CL_EVENT_COMMAND_TYPE, sizeof(cl_command_type),
                 &command, nullptr);
  cl_int status;
  clGetEventInfo(event, CL_EVENT_COMMAND_EXECUTION_STATUS, sizeof(cl_int),
                 &status, nullptr);
  const char *command_str;
  const char *status_str;
  switch (command) {
  case CL_COMMAND_READ_BUFFER:
    command_str = "buffer read";
    break;
  case CL_COMMAND_WRITE_BUFFER:
    command_str = "buffer write";
    break;
  case CL_COMMAND_NDRANGE_KERNEL:
    command_str = "kernel";
    break;
  }
  switch (status) {
  case CL_QUEUED:
    status_str = "Queued";
    break;
  case CL_SUBMITTED:
    status_str = "Submitted";
    break;
  case CL_RUNNING:
    status_str = "Executing";
    break;
  case CL_COMPLETE:
    status_str = "Completed";
    break;
  }
  printf("%s %s %s\n", status_str, reinterpret_cast<char *>(data), command_str);
  fflush(stdout);
}

// Sets the callback for a particular event
void set_callback(cl_event event, const char *queue_name) {
  OCL_CHECK(
      clSetEventCallback(event, CL_COMPLETE, event_cb, (void *)queue_name));
}

// Verify the result of the out put buffers
void verify_results(vector<vector<int>*>& iGeneratedOutputs,
									   vector<vector<int>*>& iHostInputBuffersForValidation)
{
	int kernel_nb = 0;
	for(;kernel_nb<iGeneratedOutputs.size();++kernel_nb)
	{
		for(int idx=0;idx<DATA_SIZE;++idx)
		{
			if(idx%2)
			{
				if( (*iGeneratedOutputs[kernel_nb])[idx] !=
					  (*iHostInputBuffersForValidation[kernel_nb])[idx] )
				{
					printf("Validation failed!");
					exit(EXIT_FAILURE);
				}
			}
			else
			{
				if( (*iGeneratedOutputs[kernel_nb])[idx] !=
					  40+kernel_nb )
				{
					printf("Validation failed!");
					exit(EXIT_FAILURE);
				}
			}
		}
		printf("Validation ok for kernel %d!\n",kernel_nb);
	}
}

void out_of_order_queue(xcl_world &world,
		cl_program& program,
						vector<_cl_kernel*>& iKernels,
						vector<vector<int>*>& aHostInputBuffersForValidation,
						vector<_cl_mem*>& iInputBufs,
						vector<_cl_mem*>& iOutputBufs,
                        size_t size_in_bytes)
{
  cl_int err;

  int inputBuf_size =  iInputBufs.size();
  int outputBuf_size =  iOutputBufs.size();
  int kernels_size =  iKernels.size();
  assert(inputBuf_size==outputBuf_size);
  assert(inputBuf_size==kernels_size);

  vector<cl_event> ooo_events(2*inputBuf_size);

  cl_command_queue ooo_queue = clCreateCommandQueue(
      world.context, world.device_id,
      CL_QUEUE_PROFILING_ENABLE | CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE, &err);
  check(err);

//  {
//    int zero = 0;
//    int one = 1;
//    vector<cl_event> fill_events(inputBuf_size);
//
//    for(int i=0;i<inputBuf_size;++i)
//    {
//        clEnqueueFillBuffer(ooo_queue, iInputBufs[i], &zero, sizeof(int), 0,
//                            size_in_bytes, 0, nullptr, &fill_events[i]);
//    }
//    clWaitForEvents(inputBuf_size, fill_events.data());
//    for (auto event : fill_events)
//    {
//      clReleaseEvent(event);
//    }
//  }

  vector<vector<int>*> aHostMemHandlerForOutput;

  printf("Starting out of order main loop..\n");
  for(int i=0;i<inputBuf_size;++i)
  {
	  printf("\tProcessing kernel %d..\n",i);
	  aHostMemHandlerForOutput.push_back(new vector<int>(DATA_SIZE,38));

//	  printf("\tProcessing kernel instance: %p..\n",iKernels[i]);
	  printf("\tSetting kernel %d arg 0..\n",i);
//	  printf("setting inputbuf: %p\n",iInputBufs[i]);
//	  xcl_set_kernel_arg(iKernels[i], 0, sizeof(_cl_mem*), &iInputBufs[i]);
//	  printf("\tSetting kernel %d arg 1..\n",i);
	  xcl_set_kernel_arg(iKernels[i], 0, sizeof(_cl_mem*), &iOutputBufs[i]);
	  xcl_set_kernel_arg(iKernels[i], 1, sizeof(_cl_mem*), &iInputBufs[i]);
	  xcl_set_kernel_arg(iKernels[i], 2, sizeof(cl_int), &DATA_SIZE);

	  printf("[OOO Queue]: Enqueueing kernel %d\n",i);
	  OCL_CHECK(clEnqueueNDRangeKernel(ooo_queue, iKernels[i], 1, offset, global,
	                                   local, 0, nullptr, &ooo_events[i]));
	  char kernelNamePrefix[20];
	  memset(kernelNamePrefix,0,20);
	  sprintf(kernelNamePrefix, "ffkernel_%d", i);
	  set_callback(ooo_events[i], kernelNamePrefix);

	  printf("[OOO Queue]: Enqueueing Read Buffer for kernel %d (depends on associated kernel execution)\n",i);
	  OCL_CHECK(clEnqueueReadBuffer(ooo_queue, iOutputBufs[i], CL_FALSE, 0, size_in_bytes,
			  aHostMemHandlerForOutput.back()->data(), 1, &ooo_events[i], &ooo_events[inputBuf_size+i]));
	  char kernelBufReadNamePrefix[40];
	  memset(kernelBufReadNamePrefix,0,40);
	  sprintf(kernelBufReadNamePrefix, "ffkernel_read_%d", i);
	  set_callback(ooo_events[i], kernelBufReadNamePrefix);
  }

  // Block until all operations have completed
  clFlush(ooo_queue);
  clFinish(ooo_queue);


  verify_results(aHostMemHandlerForOutput,aHostInputBuffersForValidation);

  printf("Validation ok!\n");

//  printf("Display outputs\n");
//  for(int i=0;i<inputBuf_size;++i)
//  {
//	  printf("\tdisplay output for kernel %d: \n",i);
//	  for(int idx=0;idx<DATA_SIZE;++idx)
//	  {
//		  	printf("%d ", (*aHostMemHandlerForOutput[i])[idx]);
//	  }
//	  printf("\n");
//  }

  printf("releasing stuff..");

  for(int i=0;i<inputBuf_size;++i)
  {
	  delete aHostMemHandlerForOutput[i];
  }

  for (auto event : ooo_events)
    clReleaseEvent(event);

  OCL_CHECK(clReleaseCommandQueue(ooo_queue));
}

int main(int argc, char **argv)
{
//	if(argc<=1) {
//	        printf("Needs nb of kernels to test...");
//	        exit(1);
//	}
//	 int aNb_kernels= atoi(argv[1]);
	int aNb_kernels = 16;

	printf("Starting stuff..\n");

	size_t size_in_bytes = DATA_SIZE * sizeof(int);

	vector<vector<int>*> aHostMemInputs;

  // Called to set environment variables
  xcl_world world = xcl_world_single();
  cl_program program = xcl_import_binary(world, "n_kernels");

  vector<_cl_kernel*> aKernels;

  for(int i=0;i<aNb_kernels;++i)
  {
	  char kernelName[20];
	  sprintf(kernelName, "kernel%d", i);
	  aKernels.push_back(xcl_get_kernel(program, kernelName));
	  printf("pushed  kernel: %p\n", aKernels.back());
  }
//  aKernels.push_back(xcl_get_kernel(program, "kernel0"));
//  printf("pushed  kernel: %p\n", aKernels.back());
//  aKernels.push_back(xcl_get_kernel(program, "kernel1"));
//  aKernels.push_back(xcl_get_kernel(program, "kernel2"));

  vector<_cl_mem*> aInputBuffers;
  vector<_cl_mem*> aOutputBuffers;
  for(int i=0;i<aKernels.size();++i)
   {
	  aHostMemInputs.push_back(new vector<int>(DATA_SIZE,1));

  	  aInputBuffers.push_back(xcl_malloc(world, CL_MEM_READ_ONLY, size_in_bytes));
  	  printf("Pushed read only device allocated buf: %p\n",aInputBuffers.back());
  	  xcl_memcpy_to_device(world, aInputBuffers[i], aHostMemInputs.back()->data(), size_in_bytes);
  	  aOutputBuffers.push_back(xcl_malloc(world, CL_MEM_WRITE_ONLY, size_in_bytes));
  	printf("Pushed write only device allocated buf: %p\n",aOutputBuffers.back());
   }

  // Use out of order command queue to execute the kernels
  printf("Starting out of order queuing..\n");
  out_of_order_queue(world,
		  	  	  	  	  program,
		  	  	  	  	  	  aKernels,
							  aHostMemInputs,
							  aInputBuffers,
							  aOutputBuffers,
							  size_in_bytes);

  // Release OpenCL objects
  for(int i=0;i<aInputBuffers.size();++i)
  {
	  delete aHostMemInputs[i];
	  OCL_CHECK(clReleaseMemObject(aInputBuffers[i]));
	  OCL_CHECK(clReleaseMemObject(aOutputBuffers[i]));
	  OCL_CHECK(clReleaseKernel(aKernels[i]));
  }

  OCL_CHECK(clReleaseProgram(program));
  xcl_release_world(world);

  printf("finished");

  return EXIT_SUCCESS;
}
